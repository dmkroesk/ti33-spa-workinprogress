"use strict";
// 
// Map - Reduce met mee promise: https://medium.freecodecamp.org/reduce-f47a7da511a9
//
// 1) npm init
// 2) npm --save install @types/es6-promises
// 3) tsc --init (als tsconfig.json niet bestaat)
// 4) transpile: tsc -p .
// 5) execute: node start
exports.__esModule = true;
// Functie, return Promise (promise API)
var UserRole;
(function (UserRole) {
    UserRole[UserRole["Basic"] = 0] = "Basic";
    UserRole[UserRole["Viewer"] = 1] = "Viewer";
    UserRole[UserRole["Admin"] = 2] = "Admin";
    UserRole[UserRole["SuperUser"] = 3] = "SuperUser";
})(UserRole || (UserRole = {}));
var User = /** @class */ (function () {
    function User(name, email, roles) {
        this.name = name;
        this.email = email;
        this.roles = roles;
    }
    User.prototype.hasRole = function (rolename) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            setTimeout(function () {
                var retval = _this.roles
                    .map(function (role) { return (role === rolename); })
                    .reduce(function (a, b) { return (a || b); });
                resolve(retval);
            }, Math.random() * 5000);
        });
        return promise;
    };
    ;
    return User;
}());
exports.User = User;
;
// Create 2 user
var users = [];
users.push(new User('Hendrik Haverkamp', 'hh@server.com', [UserRole.Admin, UserRole.Basic, UserRole.SuperUser]));
// users.push( new User(
//     'Diederich Kroeske',
//     'dkroeske@gmail.com',
//     [UserRole.Viewer]
//     )
// );
// Check role for single user: simple
users[0].hasRole(UserRole.SuperUser).then(function (val) { return console.log('Has role UserRole.SuperUser: ' + val); }, function (error) { return console.log('oeps'); });
// Check if both users have .Basic roles: chaining promises (achter elkaar, sequentieel)
var a;
users[0].hasRole(UserRole.Basic)
    .then(function (hasRole) {
    a = hasRole;
    return users[1].hasRole(UserRole.Basic);
})
    .then(function (hasRole) {
    var b = hasRole;
    if (a === true && b === true) {
        console.log("Chain: Both have .Basic roles!");
    }
});
// Ook kan: (parallel en onafhankelijk)
// Array van calls ...
var calls = [
    users[0].hasRole(UserRole.Basic),
    users[1].hasRole(UserRole.Basic)
];
Promise.all(calls)
    .then(function (hasRoles) {
    // Kan zo, maar grootte van array is fixed op 2
    if (hasRoles[0] === true && hasRoles[1] === true) {
        console.log("Promise.All: Both have .Basic roles!");
    }
    //In een loop? Onafhankelijk grootte array
    var isAllBasic = true;
    isAllBasic = hasRoles.every(function (hasRole) {
        return hasRole;
    });
    if (isAllBasic) {
        console.log("Promise.All met een LOOP: Both have .Basic roles!");
    }
    // Nog beter op deze manier? Onafhankelijk grootte array
    if (hasRoles.reduce(function (a, b) { return a || b; })) {
        console.log("Promise.All met Reduce: Both have .Basic roles!");
    }
});
