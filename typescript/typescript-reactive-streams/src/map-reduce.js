"use strict";
/**
 * Map-reduce example.
 */
exports.__esModule = true;
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var UserRole;
(function (UserRole) {
    UserRole[UserRole["Basic"] = 0] = "Basic";
    UserRole[UserRole["Viewer"] = 1] = "Viewer";
    UserRole[UserRole["Admin"] = 2] = "Admin";
    UserRole[UserRole["SuperUser"] = 3] = "SuperUser";
})(UserRole || (UserRole = {}));
var User = /** @class */ (function () {
    function User(values) {
        if (values === void 0) { values = {}; }
        Object.assign(this, values);
    }
    User.prototype.hasRole = function (rolename) {
        return rxjs_1.from(this.roles).pipe(
        // 1: log enum values: [0, 1, 2]
        // tap(val => console.log(val))
        // 2: map: enum => boolean
        operators_1.map(function (val) { return val === rolename; }), 
        // 3: log values: [false, true, false] 
        // tap(val => console.log(val))
        // 4: reduce: (boolean-a, boolean-b) => boolean-a || boolean-b
        operators_1.reduce(function (a, b) { return a || b; })
        // 5: reduced: [false, true, false] => [true]
        );
    };
    return User;
}());
exports.User = User;
var user = new User({
    name: 'Hendrik Haverkamp',
    email: 'hh@server.com',
    roles: [UserRole.Admin, UserRole.Basic, UserRole.SuperUser]
});
//emit array as a sequence of values
// const arraySource = from([1, 2, 3, 4, 5]);
//output: 1,2,3,4,5
// const subscribe = arraySource.subscribe(val => console.log(val));
console.log('Has role UserRole.SuperUser:');
user.hasRole(UserRole.SuperUser)
    .subscribe(function (val) { return console.log('hasRole = ' + val); });
console.log('Has role UserRole.Admin:');
user.hasRole(UserRole.Admin)
    .subscribe(function (val) { return console.log('hasRole = ' + val); });
console.log('Has role UserRole.Viewer:');
user.hasRole(UserRole.Viewer)
    .subscribe(function (val) { return console.log('hasRole = ' + val); });
