export class GeoPoint {
    constructor(public lng: number, public lat: number){}
}