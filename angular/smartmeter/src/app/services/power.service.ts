import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Power} from '../models/power.model';
import {map} from 'rxjs/operators';

const url = 'https://api.emon.ovh:8080/api';

@Injectable({
  providedIn: 'root'
})

export class PowerService {

  constructor(private http: HttpClient) {}

  getPower( signature: string, startDate: Date, endDate: Date ): Observable<Power[]> {

    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    let params = new HttpParams();
    signature ? params = params.append('signature', signature): {};
    startDate ? params = params.append('startDate', startDate.toISOString()): {};
    endDate ? params = params.append('endDate', endDate.toISOString()): {};

    return this.http.get<Power[]>(
      url + '/powerSeries',
      { headers: headers, params: params }  )
      .pipe( map( powerItem => {
          return powerItem;
        })
      );
  }
}
