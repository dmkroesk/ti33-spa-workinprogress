import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { Chart } from '../../../../node_modules/chart.js';

@Component({
  selector: 'app-daily-power-graph',
  templateUrl: './daily-power-graph.component.html',
  styleUrls: ['./daily-power-graph.component.css']
})
export class DailyPowerGraphComponent implements OnInit, OnChanges {

  @ViewChild('lineChart') private chartRef;

  chart: any;

  @Input() data: number[];

  constructor() { }

  ngOnChanges( changes: SimpleChanges ) {

    if (changes.data.firstChange === false) {
      const max = Math.max(...this.data);
      const min = Math.min(...this.data);
      console.log( max + ' ' + min );
      this.chart.data.datasets[0].data = this.data;
      this.chart.data.labels = this.data;
      this.chart.update();
    }
  }

  ngOnInit() {

    this.chart = new Chart(this.chartRef.nativeElement, {
      type: 'line',
      data: {
        labels: [], // your labels array
        datasets: [
          {
            data: this.data, // your data array
            borderColor: '#00AEFF',
            fill: false
          }
        ]
      },
      options: {
        legend: {
          display: false
        },
        scales: {
          xAxes: [{
            display: false,
            gridLines: {
              display: false
            }
          }],
          yAxes: [{
            display: false,
            gridLines: {
              display: false
            }
          }],
        }
      }
    });
  }

}
