import { Component, OnInit } from '@angular/core';
import {PowerService} from '../services/power.service';
import {interval} from 'rxjs';

@Component({
  selector: 'app-daily-power',
  templateUrl: './daily-power.component.html',
  styleUrls: ['./daily-power.component.css']
})
export class DailyPowerComponent implements OnInit {

  powerValues: number[];

  constructor(private powerService: PowerService) { }

  ngOnInit() {
   interval(5000).subscribe( () => {
      this.powerService.getPower(
        '00000000d926eb43', // Signature
        new Date('2019-02-27T00:00:00.000Z'), // Startdate
        new Date('2019-02-27T23:59:00.000Z')  // Enddate
        ).subscribe( power => {
          this.powerValues = power.map( (item) => {
              return (item.p1.instantaneous_active_power[1].value -
              item.p1.instantaneous_active_power[0].value) * 1000;
          });
          console.log(this.powerValues);
      });
    });
  }


}
