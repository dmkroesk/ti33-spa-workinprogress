import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import { DailyPowerComponent } from './daily-power/daily-power.component';
import { DailyPowerGraphComponent } from './daily-power/daily-power-graph/daily-power-graph.component';

@NgModule({
  declarations: [
    AppComponent,
    DailyPowerComponent,
    DailyPowerGraphComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
