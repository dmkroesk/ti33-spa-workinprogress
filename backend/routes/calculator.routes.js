'use strict';

let routes = require('express').Router();
const CalculatorController = require('../controllers/calculator.controller');

// Berichten CRUD
routes.post('/calculator', CalculatorController.performCalculation );
routes.get('/calculator', CalculatorController.getCalculations );

module.exports = routes;