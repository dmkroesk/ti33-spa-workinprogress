const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const DeelnemerSchema = new Schema({
    naam: String,
    inleg: Number
});

const CalculationJobSchema = new Schema({
        info: String,
        event: String,
        deelnemers: {
            type: [DeelnemerSchema]
        }
    },
    {
        timestamps: true,

        toJSON: {
            transform: (doc, ret) => {
                delete ret._id;
                delete ret.__v;
            }
        }
    }
);

module.exports = mongoose.model('verreken_job', CalculationJobSchema);