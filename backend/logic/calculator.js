'use strict';
const {KasBoekItem, KasBoek} = require('../models/Kasboek');

class Calculator {

    constructor() {
    }

    calculate(deelnemers) {

        const totaal = deelnemers.map(item => item.inleg).reduce((acc, val) => acc + val, 0);

        const dn = deelnemers.filter(item => Math.abs((item.inleg * deelnemers.length) - totaal) >= 0.001);

        const kasboek = new KasBoek();
        kasboek.totaal = totaal;

        for (const idx of dn) {
            for (const idy of dn) {
                if (idx !== idy) {
                    const betaling = (idx.inleg / dn.length) - (idy.inleg / dn.length);
                    if (betaling > 0) {
                        let item = new KasBoekItem();
                        item.van = idy.naam;
                        item.naar = idx.naam;
                        item.bedrag = betaling;
                        kasboek.items.push(item);
                    }
                }
            }
        }
        return kasboek;
    }
}

module.exports = Calculator;
