//
// CRUD operations on person
//

'use strict';

const ApiError =  require('../ApiError');
const Calculator = require('../logic/calculator');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const CalculationJobSchema = require('../database/calcjob.schema');

module.exports = {

    performCalculation( req, res, next ) {
        let job = new CalculationJobSchema(req.body);
        job.save()
            .then( user => {
                const calc = new Calculator().calculate(user.deelnemers);
                res.status(200).json(calc).end()
            } )
            .catch( error => new ApiError(error.toString(), 500));
    },

    getCalculations( req, res, next ) {

        let query = {}

        CalculationJobSchema
            .find(query, {})
            .sort('-createdAt')
            .limit(200)
            .then( (rows) => {
                res.status(200).json(rows);
            })
            .catch( (error) => {
                next(new ApiError(error.toLocaleString(), 500))
            });

    }
};