'use strict';

class KasBoekItem {
    constructor() {
        this.van = 0;
        this.naar = 0;
        this.bedrag = 0;
    }
}

class KasBoek {
    constructor() {
        this.totaal = 0;
        this.items = [];
    }
}

module.exports = {
    KasBoekItem,
    KasBoek
};

